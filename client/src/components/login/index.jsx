import { React, useState } from 'react'
import Login from './login'
import Signup from './signup'
import Greeting from './greeting'
import './style.scss'

function Index() {

    const [isLoginForm, setIsLoginForm] = useState(true)

    console.log(`index state isLogin form: ${isLoginForm}`)

    function switchForm() {
        setIsLoginForm((isLoginForm) => !isLoginForm)
    }

    return (
        <div className="log-section"> {/*just cover for center form on screens*/}
            <div className="sign-form-wrap">
                <Login/>
                <Signup/>
                <Greeting isLoginForm={isLoginForm} switchFormHandler={switchForm}/>
            </div>
        </div>
    )
}

export default Index
