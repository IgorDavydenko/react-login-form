import React from 'react'
import './style.scss'
import Social from './social'

function Signup() {
    return (
        <div className="sign-form__container sign-up__container">
            <form action="#">
                <h1>Sign up</h1>
                <Social/>
                <p>or use your email for registration</p>
                <input type="text" placeholder="@email"></input>
                <input type="email" placeholder="@email"></input>
                <input type="password" placeholder="password"></input>
  
                <button className="sign-form__button sign-form__button_sign-up" type="submit">sign up</button>
            </form>
        </div>
    )
}

export default Signup
