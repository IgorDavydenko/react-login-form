import React from 'react'
import './style.scss'
import Social from './social'


function Login() {
    return (
        <div className="sign-form__container sign-in__container">
            <form action="#">
                <h1>Sign in</h1>
                <Social/>
                <p>or use your email account</p>
                <input type="email" placeholder="@email"></input>
                <input type="password" placeholder="password"></input>
                <button type="submit">sign in</button>
                <a className="sign-form__pw-forgot" href="#">Forgot your password?</a>
            </form>
        </div>
    )
}

export default Login
