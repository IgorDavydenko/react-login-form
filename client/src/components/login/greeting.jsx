import React from 'react'
import './style.scss'

function Greeting({ isLoginForm, switchFormHandler}) {



    return (
        <div className="greeting-container">
            <div className="greeting">
                <div className={`greeting-container__sign sign-in-greeting ${isLoginForm ? "" : "form__sig-in_active"}`}>
                    <h1>Welcome Back!</h1>
                    <p>Please login with your personal info.</p>
                    {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> */}
                    <button className="greeting__button" type="submit" onClick={switchFormHandler} >sign in</button>
                </div>
                <div className="greeting-container__sign sign-up-greeting">
                    <h1>Glad to see you here!</h1>
                    <p>Please, enter your personal details before start</p>
                    {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> */}
                    <button className="greeting__button" type="submit" onClick={switchFormHandler}>sign up</button>
                </div>
            </div>
        </div>
    )
}

export default Greeting
